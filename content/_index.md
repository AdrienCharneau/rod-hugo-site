---
title: Home
type: docs
bookToc: false
---

[![*Cover*](https://i.imgur.com/9kdaEBE.jpg)]({{< ref "/_index.md" >}})








# **Welcome**

***




*Rise of Dissent* is a work-in-progress tabletop RPG, an attempt at simulating the effects of emotional strain in a fictional setting. Characters are thrown into a harsh environment, working towards achieving a group task (usually assigned by a powerful entity, like a guild or a ruler), while also trying to stay cool and advance their own [personal goals]({{< ref "/docs/game-rules/characters/archetypes.md" >}}). These dynamics shape their actions, decisions and reactions to the challenges they face.

<!-- In a perfect version, characters should encounter opportunities for personal growth, learn to overcome their differences and build resilience in the face of adversity. -->

#### Gameplay

This game combines elements of *Blades in the Dark*, *Masks*, and *D&D*, with a strong emphasis on world exploration and adventuring. It focuses less on dungeons or battles though, and more on turning ordinary situations into compelling objectives (with players having to manage their character's [emotions]({{< ref "/docs/game-rules/core-mechanics/breakdown-state.md" >}})). [Action resolution]({{< ref "/docs/game-rules/core-mechanics/action-rolls.md" >}}) combines two [abilities]({{< ref "/docs/game-rules/characters/abilities.md" >}}), incorporates *narrative risk* (or *positioning*) and a push your luck mechanic. Gameplay also tries to keep its crunch and statistics to a minimum, focusing heavily on [progress clock]({{< ref "/docs/game-rules/core-mechanics/progress-clocks.md" >}}) to streamline the GM's experience.

>*Read the rules in [PDF format](https://drive.google.com/drive/folders/1nSUHHWyH__IH1hAKKcZYMl8bjJ5p-xy7).*

>*Full list of inspirations can be found in the [Credits]({{< ref "/docs/project/credits.md" >}}) section.*

#### Setting

Lore and worldbuilding are not available yet. But in the long run I want this project to focus on conflicts between factions and explore themes like radicalisation or colonialism. The mechanics can be adapted into any setting though, so feel free to use them as you like.

<!-- With that say, I also want to try my best not to glorify war, obsess over military tactics or romanticize insurgency or terrorism. Conflict is ugly. People in charge will abuse their power or turn a blind eye to cruelty. People with a just cause will hurt innocents and perpetrate a cycle of violence. -->

#### *Anyway, thanks a lot for your time and see you around!*

- [**Subscribe to Newsletter**](https://docs.google.com/forms/d/15UsLI4rcp4anxRSx5_aREX8q9LqFAlRMhcAezaSjuho)

- [**Provide Feedback**](https://docs.google.com/forms/d/1Z-Cb9QrMBZ6Yr3_bMIHpZ9L-A3lf8dDxu9DpoZyjvO4)




<!-- ## Newsletter & Feedback


Stay tuned for regular updates and subscribe to the [Newsletter](). And your opinion is valuable, so feel free to share any [Feedback](https://docs.google.com/forms/d/1_YnzjEypyIPDtyaKIhlLCClImKlV_0pPUuq_DQ9mtN8). -->

***








# **News**

***




## Website is live!


### *05/25/2024*

![blog - 2024-05-25](https://i.imgur.com/z6UkULM.jpg)

After months of writing and development, I am excited to announce the official launch of this website. This will be the central hub for all things related to *Rise of Dissent*. Here, you'll find comprehensive [Game Rules]({{< ref "/docs/game-rules/introduction.md" >}}), lore and updates about the project.

#### What am I currently working on?

- Character flaws, social influence and combat mechanics plus a few other rules.

- A module for playtesting.

- Early character sheet draft.

- Researching and taking notes for worldbuilding.

***








[**<- Previous**]({{< ref "/docs/project/news/- 2024/05-25.md" >}})
