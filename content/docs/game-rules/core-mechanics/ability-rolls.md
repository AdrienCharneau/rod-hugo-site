---
title: Ability Rolls
bookToc: false
draft: false
weight: 1
---

[![*Cover*](https://i.imgur.com/9kdaEBE.jpg)]({{< ref "/_index.md" >}})








# **Ability Rolls**

***




**Ability Rolls** are a simple way to test a character's **Abilities**. This is when the GM needs to resolve an outcome where a character assumes a passive position, where they aren't actively resisting something, or aren't carrying out a deliberate action. For example, an **Ability Roll** might be used to see if a character notices something unusual in their surroundings, or manages to recall a specific piece of information. But not if they attempt to climb a wall or dodge an attack.

>*Ability Rolls are typically made by players at the GM's request. However, the GM may sometimes roll in secret to keep players unaware.*




## Steps


### - Choosing an Ability

When an **Ability Roll** is called, the GM must first pick an **Ability** for it. Here are some examples:

- **Dexterity** : for determining if a character hears something unusual or spots a hidden object.

<!-- Charm : for determining an NPC's first impression of the character. -->

- **Knowledge** : for determining if a character recognizes an item, symbol, spell or anything that requires education or expertise.

<!-- Reasoning : for orientating oneself or finding direction in unknown territory. -->

- **Insight** : for figuring out a person and/or their ulterior motives in a conversation.

etc...

>*Depending on its score, the Ability determines which type of die is rolled.*


### - Resolution

The player (or GM) rolls the die. Any result of 3 or more is considered a *success*, a result of 1 or 2 is considered a *failure*.




## Example


Finn is observing a fortress from a safe distance, planning their next move. They notice movement on the walls. The player controlling the character asks if they can see anything:

- The GM tells the player to roll a *d6* (based on Finn's *dexterity* of 2), as they need to test Finn's eyesight.

- They get a result of 5, which counts as a *success*. The GM says that Finn notices five guards: three patrolling the walls and two standing on a tower.

***








[**<- Previous**]({{< ref "/docs/game-rules/characters/template-characters.md" >}}) | [**Next ->**]({{< ref "/docs/game-rules/core-mechanics/action-rolls.md" >}})