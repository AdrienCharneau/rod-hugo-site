---
title: Progress Clocks
bookToc: false
draft: false
weight: 3
---

[![*Cover*](https://i.imgur.com/9kdaEBE.jpg)]({{< ref "/_index.md" >}})








# **Progress Clocks**

***




A **Progress Clock** is a gauge (or circle) divided into sections used to trace almost anything. This could be plot progression, time management, NPC health or any other variable essential to the unfolding of the narrative. It can be compared to a "progress bar" found in video games, but adapted to suit a variety of situations.

>*Progress Clocks transform narrative outcomes into something tangible that players can grasp, whether it's overcoming an enemy, escaping danger or fulfilling an objective.*

The greater the **Progress Clock**, the more sections it holds:

|Progress Clock |Sections |
|:-------------:|:-------:|
|Standard       |2        |
|Large          |4        |
|Huge           |6        |
|Mighty         |8        |

A **Progress Clock** should always reflect what's happening in the fiction, allowing players to check how they’re doing. For example:

- **Event Clocks** are used to track the progression of time. They can represent the countdown to an event, the arrival of reinforcements or the duration of a spell. Each segment should mark a specific time interval, enabling players to anticipate future developments or prepare accordingly.

- **Obstacle Clocks** are used to track ongoing effort against a challenge. Characters tick segments when succeeding **Action Rolls** aiming at overcoming the challenge, until the clock is filled and the obstacle is no more. Unless it's a *critical success*, a single action cannot fill more than one segment at a time.

<!-- When you create a clock, make it about the obstacle, not the method. The clocks for an infiltration should be “Interior Patrols” and “The Tower,” not “Sneak Past the Guards” or “Climb the Tower.” The patrols and the tower are the obstacles— the PCs can attempt to overcome them in a variety of ways. -->

- **Threat Clocks** are used to monitor impending trouble or danger. They can symbolize growing suspicion, the proximity of pursuers or the alert level of guards. Segments are ticked when complications arise or characters mess up, like when an action ends on a *limited success* or a *failure* (with *critical failures* ticking more than one segment at a time).

- **Combat Clocks** are used to track the progression of combat encounters. They can represent a number of enemies, their morale, their health or even their armor. Segments  are ticked when characters perform successful attacks on their adversaries and, unless there's a *critical success*, a single attack cannot fill more than one segment at a time.

- **Influence Clocks** are used to represent the effort required to sway an opinion, negotiate a deal or achieve an objective through persuasion, intimidation or manipulation. Segments can be ticked based on the narrative, or through the success of social actions aimed at influencing the target.

- **Project Clocks** are used to monitor the advancement of a long-term task or endeavor. This could be building a stronghold, learning a skill, writting a book or crafting an artifact. Segments should represent the various stages of the project, with each tick marking a significant milestone towards completion.

>*Progress Clocks are not required for every situation. The GM should use them for challenges where tracking advancement is essential. Otherwise, they should opt for resolving outcomes with a single Action Roll.*

<!-- Faction Clock : each faction has a long-term goal. When the PCs have downtime, the GM ticks forward the faction clocks that they’re interested in. In this way, the world around the PCs is dynamic and things happen that they’re not directly connected to, changing the overall situation in the city and creating new opportunities and challenges. The PCs may also directly affect NPC faction clocks, based on the missions and scores they pull off. Discuss known faction projects that they might aid or interfere with, and also consider how a PC operation might affect the NPC clocks, whether the players intended it or not. -->

<!-- Spellcasting Clock -->

<!-- When you create a clock, make it about the obstacle, not the method. The clocks for an infiltration should be “Interior Patrols” and “The Tower,” not “Sneak Past the Guards” or “Climb the Tower.” The patrols and the tower are the obstacles —the PCs can attempt to overcome them in a variety of ways. -->

<!-- https://www.reddit.com/r/bladesinthedark/comments/u2na85/help_with_understanding_progress_clocks -->

<!-- https://www.reddit.com/r/rpg/comments/bft68t/using_clocks_world_bitd_in_any_game/ -->

***








[**<- Previous**]({{< ref "/docs/game-rules/core-mechanics/action-rolls.md" >}}) | [**Next ->**]({{< ref "/docs/game-rules/core-mechanics/resistance-rolls.md" >}})
