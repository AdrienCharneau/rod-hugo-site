---
title: Resistance Rolls
bookToc: false
draft: false
weight: 4
---

[![*Cover*](https://i.imgur.com/9kdaEBE.jpg)]({{< ref "/_index.md" >}})








# **Resistance Rolls**

***




**Resistance Rolls** represent a character's attempt at withstanding undesirable events, rather than them actively initiating a course of action. Players may call for one anytime they want to negate something affecting their character; with the roll always succeeding provided the character isn't in a **Breakdown** state. For instance, if a character is struck by an enemy, they may perform a **Resistance Roll** to dodge the attack.

>*However, the decision on what can or can't be resisted is left at the GM's discretion. And they reserve the right to deny a player's request if deemed inappropriate. The GM may also suggest a player to perform a Resistance Roll at any point during a game, but cannot enforce it.*

<!-- This is when a character assumes a reactive position against a situation or circumstance. -->

<!-- Players always drive the action, making all the dice rolls. -->


### - Choosing an Ability

If a **Resistance Roll** is initiated, the GM must first pick an appropriate ability for it. Here are some examples:

- **Dexterity** : when reacting instinctively to danger.

- **Agility** : when dodging incoming threats or attacks.

- **Knowledge** : when refuting attempts at persuasion based on misinformation or quackery.

- **Insight** : when refuting attempts at persuasion based on trickery or lies.

- **Composure** : when refuting temptation or seduction.

- **Endurance** : when resisting complications arising from wounds or diseases.

etc...

>*Depending on its score, the Ability determines which type of die is rolled.*


### - Assessing Pressure

**Pressure** measures how much strain is being exerted on the character. It's a representation of the level of effort or resolve needed to resist the event:

|Pressure |Rating |Example |
|:-------:|:-----:|:-------|
|Moderate |1-2    |The burden on the character is manageable. |
|Severe   |3-4    |The burden significantly challenges the character's capabilities. |
|Extreme  |5-6    |The burden is overwhelming and pushes the character to their limits. |

>*The GM sets this value, which in turn determines the number of die rolled by the player.*


### - Resolution

The player rolls a number of **Ability** die equal to the roll's **Pressure**, with results of 1 and 2 counting as **Trouble Die**. Subsequently, the character raises one of their **Emotions** by the amount of **Trouble Die** obtained, and the event or consequence is negated.

<!-- Talk about Modifiers here...? -->

<!-- Anticipation/Alertness : if their character isn't in a Breakdown state, a player can choose to "predict" a future negative event. If so, they may raise one of their Emotions by 1 and, if the "predicted" event happens, their character succesfully resists it without having to perform a Resistance Roll. -->

<!-- When your PC suffers a consequence that you don’t like, you can choose to resist it. Just tell the GM, “No, I don’t think so. I’m resisting that.” Resistance is always automatically effective—the GM will tell you if the consequence is reduced in severity or if you avoid it entirely. Then, you’ll make a resistance roll to see how much stress your character suffers as a result of their resistance. -->

<!-- ### - Example -->

<!-- Spire - The City must Fall: -->

<!-- Stunned (Blood) : You take a blow to the head, or are winded, giving your enemies opportunity to act. If they want to get away from you, they can do so while you stagger about and gather your senses; otherwise, you can’t use the Fight skill to earn additional dice for the remainder of the situation. -->

<!-- Compromised (Shadow) : A friendly NPC asks you to justify your strange behaviour. -->

<!-- Pawned (Silver) : Until the end of the next session, you lose the use of one piece of equipment that’s important to you. -->

<!-- Knocked Out (Blood) : You fall unconscious for several hours, during which time your enemies get an advantage. -->

<!-- Leak (Moderate) : Your bond unwittingly gives out information that threatens the operation. Mark D6 stress in Shadow. -->

<!-- Passive Rolls (or "Ability Checks") work like Resistance Rolls, except they don't incur negative outcomes for the character if failed, and the character can't "negate" their failure by raising their Emotional Strain. -->

<!-- Insight : used when sensing ulterior motives or hidden agendas in people (without actively engaging in conversation). -->

#### Raising Emotion

As stated above, characters must raise an **Emotion** when obtaining **Trouble Die**. Which **Emotion** to raise depends on context, and the character's general mindset at the time of the roll:

|Emotion     |Description |
|:----------:|:-----------|
|Anger       |The character feels personally wronged or threatened by the event. |
|Frustration |The character feels irritated by the unexpected or displeasing event. |
|Anxiety     |The character feels vulnerable or in danger. |
|Guilt       |The character feels somewhat responsible for the event affecting them. |

<!-- Once raised, Emotions cannot be lowered. But they can be cleared entirely if the character manages to fulfil (or at least significantly pursue) their current Quest. Or if they choose to leave the group (for a full day at least) while the plot progresses without them. -->

<!-- provide example(s)? -->

<!-- Anger : attributing the cause of harm to someone else and believing they can still influence the situation through assertiveness or confrontation -->

<!-- Frustration : the character feels irritated due to unexpected events, unwanted tasks, or outcomes that didn't go their way. Their response is usually one of annoyance or bitterness. -->

<!-- Insecurity : the character perceives themselves as inadequate, doubting their own abilities due to underestimating an obstacle or feeling embarrassed in a social situation. -->

<!-- Falling prey to a thief : a situation where a minor theft occurs, involving the stealing of coins from a character. While somewhat inconsequential, it can still generate a sense of distress and insecurity to the character. -->

<!-- Social embarrassment : a character experiences a situation where they are publicly embarrassed or humiliated, such as being the target of a practical joke in front of important figures. This can lead to a significant damage to their self-esteem. -->

<!-- Fear : the character senses danger, perceiving it as insurmountable or mysterious. They feel like they can't deal with it or comprehend its origin or nature. -->

<!-- Guilt : the character feels responsible for their own actions, acknowledging wrongdoing or believing that the current situation arose from their own choices. -->

<!-- Spire - The City Must Fall : Sometimes it will be stated outright what kind of stress a situation does out – for example, when a Lajhan casts a spell, most often they’ll be asked to mark stress against Mind as they channel the vast energies of their goddess. If it’s not clear where stress would go, the GM and the player can work it out together. -->

<!-- Example of Anger : a situation where a character perceives like they are being treated unfairly or if they get robbed of their belongings while walking in a city. -->

<!-- Example of Insecurity : being anti-theist and participating in a wedding where religious people are having lots of fun. People not giving you due attention or respect etc... -->

<!-- Anger : the character is convinced someone or something else is responsible for the stress increase and feels like they can still influence the situation or cope with it. -->
<!-- Shame : the stress increase was caused by an event that was initiated by the character's own choices and feels like they cannot influence the situation or cope with it. -->
<!-- Anxiety : the character is convinced someone or something else is responsible for the stress increase and feels like they cannot influence the situation or cope with it. -->
<!-- Guilt : the stress increase was caused by an event that was initiated by the character's own choices and feels like they can still influence the situation or cope with it. -->




## Example


Finn wants to evade an attack performed by a rival character:

- Considering Finn's animosity towards the character, the GM states that this may raise Finn's *anger*, and test his *agility* against a **Pressure** of 2.

- Finn has an agility score of 3, so the player rolls two *d8s*.

- They get results of 4 and 2, with 2 counting as one **Trouble Die**. Finn's *anger* is increased by 1 and the attack is succesfully dodged.

***








[**<- Previous**]({{< ref "/docs/game-rules/core-mechanics/progress-clocks.md" >}}) | [**Next ->**]({{< ref "/docs/game-rules/core-mechanics/breakdown-state.md" >}})
