---
title: Breakdown State
bookToc: false
draft: false
weight: 5
---

[![*Cover*](https://i.imgur.com/9kdaEBE.jpg)]({{< ref "/_index.md" >}})








# **Breakdown State**

***




Once a character's **Emotion** reaches its maximum threshold, they enter what is called a **Breakdown** state. They are now subjected to a specific set of rules, cannot perform **Resistance Rolls**, and none of their **Emotions** can be raised.

<!-- Smallville - Stressing Out (Corebook p.60) : Stressing Out can mean many things. It may simply mean you fall unconscious. It may mean you collapse in doubt and angst. It may mean you stalk out of the room before you hurt someone. It may mean you stare out a window plotting revenge. When you Stress Out, it’s up to you to decide what it means in the story. Whatever the details, though, you’re out for the rest of the scene. -->

<!-- Stress/Panic table mechanic found in Alien RPG: https://www.youtube.com/watch?v=H16lBiBSd-I -->
<!-- Check DATA/Resources/GAMEPLAY/TTRPG inspirations/[TO READ]/Year Zero Engine/ -->
<!-- https://online.anyflip.com/aeoqf/frgt/mobile/index.html -->
<!-- https://www.drivethrurpg.com/product/293976/ALIEN-RPG-Core-Rulebook?cPath=27806_34036 -->
<!-- https://www.legrog.org/jeux/alien -->

#### Action Rolls

Each time the player opts for a **Daring Roll**, instead of raising an **Emotion**, they must increase their character's **Stress** by 1. Additionally, they roll with **Disadvantage** on actions related to the **Emotion** that triggered the **Breakdown**:

|Emotion     |Description |
|:----------:|:-----------|
|Anger       |The character rolls with **Disadvantage** if they're trying to help, comfort or support someone. |
|Frustration |The character rolls with **Disadvantage** if they're trying to problem-solve or focus on intellectual tasks. |
|Anxiety     |The character rolls with **Disadvantage** if they're trying to directly engage an obstacle or threat. |
|Guilt       |The character rolls with **Disadvantage** if they're trying to hurt or provoke someone. |

<!-- Characters suffering a Breakdown also make things difficult for everyone else around them. This translates by worsening the Environment by 1 of all actions performed by allies nearby? -->

#### Struggle Rolls

If the player chooses to resist an  event or circumstance, they must perform a **Struggle Roll** instead of a **Resistance Roll**. This also involves picking an **Ability** and a **Pressure** rating, but only negates the event if no **Trouble Die** are rolled.

>*If the player rolls at least one Trouble Die, the Struggle Roll fails, the event or circumstance still occurs, and the character's Stress increases by the total number of Trouble Die rolled.*

<!-- Provide Example -->

#### Fallout

In a **Breakdown** state, if a character's **Stress** reaches or overcomes their current **Health**, they are taken out of action. They cannot continue as an adventurer and must retire to a different life.

<!-- Direct Stress Damage? -->

<!-- Heavy material loss : a character experiences a significant loss of possessions or resources that hold personal value or importance to them. This can include strong financial setbacks, damage to important tools or loss of cherished belongings. -->

<!-- Betrayal of trust : a close friend or ally betrays a character's trust, either by revealing sensitive information or turning against them for personal gain. This can be emotionally devastating, causing the character to question their judgment and potentially leading to a loss of friendship. -->

<!-- Death of a loved one : the loss of a close friend or parent has a profound impact on the character's emotional well-being. Grief and sadness can accumulate significant permanent stress, potentially affecting the character's ability to function optimally or make rational decisions. -->




## Overcoming a Breakdown


To remove their **Breakdown** state, a character must address the **Emotion** that triggered it and do something impactful with it. Whatever that is should affect the course of the story:

|Emotion     |Action description |
|:----------:|:-----------|
|Anger       |The character must hurt someone or break something important. |
|Frustration |The character must fling themselves into cheap relief, or take a foolhardy action that puts the group in a difficult situation. |
|Anxiety     |The character must give up an important task or desert the group at a critical moment. |
|Guilt       |The character must make an important sacrifice to redeem themselves. |

>*What is considered impactful is left to the GM's discretion.<!-- If this action requires performing an Action Roll, the character also rolls with Advantage.-->*

Alternatively, a **Breakdown** state can also be removed if the character manages to advance their current **Quest**, or takes a temporary leave from the group (for a full day at least, while the plot progresses without them).

>*Upon removing a Breakdown, all of the character's Emotions are set back to 0. And any steps described here can be repeated if the Character isn't in a Breakdown state, for the sole purpose of clearing out Emotions.*

<!--  -->

<!-- Characters should also be able to recover (1? 2?) Emotional Strain twice a day while eating or resting. -->

<!-- Characters should be able to recover Emotional Strain in downtime or by bonding with each other? The amount of emotional recovery should be dependent on the overall Empathy score for the group? (+Emotional Breakdowns should reduce the recovery rate -characters with a low Empathy score should even increase Emotional stress during downtime?) -->

***








[**<- Previous**]({{< ref "/docs/game-rules/core-mechanics/resistance-rolls.md" >}})
