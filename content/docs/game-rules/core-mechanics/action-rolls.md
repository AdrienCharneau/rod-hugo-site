---
title: Action Rolls
bookToc: false
draft: false
weight: 2
---

[![*Cover*](https://i.imgur.com/9kdaEBE.jpg)]({{< ref "/_index.md" >}})








# **Action Rolls**

***




An **Action Roll** is a roll where a character takes a deliberate action, initiating a specific course of events or using their **Abilities** to achieve something. The GM should only require this type of roll if something is at stake, or there's a possibility for a bad outcome<!-- (no matter how small it is)-->. It shouldn't be required for routine or very simple tasks either.

<!-- Many roleplay games have the concept of "failing forward". This means that every roll has some consequence which is usually narrated by the GM and usually bad for the character or the party as a whole. A roll without possible negative consequences should not be made.

Every moment of play, roll dice or say “yes.”

If nothing is at stake, say “yes” [to the player’s request], whatever they’re doing. Just go along with them. If they ask for information, give it to them. If they have their characters go somewhere, they’re there. If they want it, it’s theirs.

Sooner or later—sooner, because [your game’s] pregnant with crisis— they’ ll have their characters do something that someone else won’ t like. Bang! Something’s at stake. Start the confl ict and roll the dice.

Roll dice, or say “yes.” -->




## Steps


### - Declaring the Action

To perform an **Action Roll**, the player describes their objective and states the means by which they plan to achieve it. When necessary, it's the GM's job to ask and clarify this step, to ensure a clear understanding of the character's intentions and methods.

>*For example, while both "successfully casting a spell" and "successfully casting a spell to impress a powerful sorcerer" may seem like identical tasks, their underlying intentions and expected outcomes differ significantly.*


### - Assessing Challenge

Set by the GM, **Challenge** gauges the amount of effort or skill required to succeed the roll:

|Challenge |Rating |Example |
|:--------:|:-----:|:-------|
|Average   |2      |The action is straightforward but requires some level of effort or skill. Examples include sneaking past a distracted guard, performing a basic first aid procedure or walking across a sturdy plank of wood. |
|Difficult |3      |The action is tough and requires considerable levels of effort or skill. Examples include deciphering a complex code, climbing a steep wall or playing a convoluted musical piece on the piano. |
|Daunting  |4      |The action is excessively hard and requires exceptional levels of effort or skill. Examples include sneaking undetected past a heavily guarded perimeter, performing an advanced surgical procedure or walking on a tightrope. |

Depending on context, the GM may use these parameters as a guideline for setting a **Challenge**:

- **Environment** : the physical or social space in which the action takes place.

- **Actors** : is the action being helped or opposed by other people.

- **Means** : the resources, items, tools or pieces of knowledge available.

- **Condition** : the general state of the character performing the action, including their physical and mental health.

<!-- By default, all Action Rolls have a Challenge of 3. The GM may also hide this value from players if they feel like it fits the situation. -->

<!-- They may even go as far as to ask for a simple Insight or Knowledge Roll/ability check to reveal it. -->


### - Assessing Risk

Also set by the GM, **Risk** indicates potential danger, uncertainty or threat associated with the action. This value represents the likelihood of a troublesome outcome; with the greater the likelihood, the lower the value:

|Risk       |Rating |Example |
|:---------:|:-----:|:-------|
|Safe       |4      |The action presents minimal danger, granting the character a good opportunity to act. Examples include shooting atop a fortification through a murder hole, striking a crippled adversary or breaking into an unguarded warehouse. |
|Risky      |3      |The action is hazardous or involves potential harm, forcing the character to gamble their outcome. Examples include handling volatile explosives, attempting to traverse a crumbling bridge or trying to deceive a guard to infiltrate a restricted area. |
|Precarious |2      |The action hangs on a knife's edge, with the character facing imminent trouble. Examples include confronting a powerful enemy head-on, running accross a booby trapped minefield or trying to rescue hostages from a burning building.|

<!-- The examples written here have been copy-pasted from Blades in the Dark. -->

<!-- By default, all Action Rolls have a Risk of 3. The GM may also hide this value from players if they feel like it fits the situation. -->

Depending on the circumstances, **Risk** can be placed into one of four categories:

- **Narrative Risk** : plot-related consequences, involving complications or setbacks that impact the adventure or current situation <!-- some of which could induce Stress on the character..? -->.

- **Physical Risk** : consequences related to the physical well-being of the character, including loss of **Health** caused by injuries or disease.

- **Material Risk** : consequences related to possessions or resources, including the loss or damage of items, equipment or valuables.

- **Social Risk** : consequences pertaining to social rank, status and interactions, such as damaged reputation or loss of *respect*/*sympathy* from other characters.

<!-- Time Risk : consequences related to time, including missing a deadline or falling prey to the night. -->

<!-- The GM should never inflict a consequence that completelly negates the success of an action. -->

<!-- Don't inflict a complication that negates a successful roll. If a PC tries to corner an enemy and gets a 4/5, don't say that the enemy escapes. The player's roll succeeded, so the enemy is cornered... maybe the PC has to wrestle them into position and during the scuffle the enemy grabs their gun. -->


### - Choosing Abilitie Die

**Action Rolls** always combine two **Abilities**, based on the specifics of the action. For example, *agility* and *endurance* may be relevant for a climb roll, while *reasoning* and *creativity* may be relevant for a problem-solving roll. Here are a few other examples:

- **Brewing a potion** may require *knowledge* and *reasoning*, as it involves understanding the properties of various ingredients, as well as the proper techniques for preparing and mixing them.

- **Writing a poem** may require *eloquence* and *creativity*, as it involves using language and expression in imaginative ways to convey emotions and ideas.

- **Swimming** may require *agility* and *endurance*, as it involves the ability to move efficiently through water and sustain prolonged physical effort.

- **Painting** may require *creativity* and *dexterity*, as it involves the ability to express oneself through artistic mediums, and the manual skill to execute the desired strokes and forms.

- **Solving a puzzle** may require *reasoning* and *creativity*, as it involves analyzing available information and coming up with a unique solution.

- **Lockpicking** may require *dexterity* and *reasoning*, as it involves manipulating intricate mechanisms with precision, while also analyzing components and identifying potential vulnerabilities.

<!-- Walking across a wooden plank may require agility and composure, as it involves maintaining balance and control while navigating a narrow, precarious path. -->

etc...

>*Depending on their scores, these Abilities determine which type of die are rolled, with 2 die used for each Ability (4 in total). For instance, if a character wants to swim accross a river, and they have an endurance score of 3 and an agility score of 1, they pick two d8s and two d4s. If they want to solve a puzzle, and they have a reasoning score of 2 and a creativity score of 2, they pick four d6s etc...*


### - Resolution

Resolving an **Action Roll** involves two steps: a standard roll followed by an optional **Daring Roll**, with players trying to maximize **Success Die** while also minimizing **Trouble Die**:

- The player throws their 4 die.

- Results of 4 or more are considered **Success Die**. Results of 3 are ignored and results of 1 and 2 are considered **Trouble Die**.

- The player can choose to end their action after the first throw, or proceed with a **Daring Roll**.

- Opting for the latter raises one of the character's **Emotion** by 1, and allows them to roll 2 additional die (one for each **Ability**), aiming to gain more **Success Die** while also risking additional **Trouble Die**.

>*After a Daring Roll, a player should have rolled 6 die in total. If not, they should have rolled only 4 die.*

<!-- Alternative : Opting for the latter raises one of the character's Emotion by an amount dependent on the roll's Challenge (Average = 1, Difficult = 2, Overwhelming = 3) -->

#### Raising Emotion

As stated above, a character must raise an **Emotion** by one stage when attempting a **Daring Roll**. Which **Emotion** to raise is determined by context and the character's general mindset while performing the action:

|Emotion     |Description |
|:----------:|:-----------|
|Anger       |The character is acting out of resentment or animosity. They feel wronged or threatened, and think that they can overcome the obstacle through assertiveness or confrontation. |
|Frustration |The character is acting out of annoyance or irritation. Things aren't turning out as expected, they're dealing with an unforeseen obstacle or an unwanted task. |
|Anxiety     |The character is acting with uncertainty or apprehension. They are plagued by fear, second-guessing their decision or perceiving themselves as too weak for the task. |
|Guilt       |The character is acting against their own values or principles. They feel responsible for what they're doing or believe that they shouldn't be doing it in the first place. |

<!-- Once raised, Emotions cannot be lowered. But they can be cleared entirely if the character manages to fulfil (or at least significantly pursue) their current Quest. Or if they choose to leave the group (for a full day at least) while the plot progresses without them. -->


### - Outcomes

Action outcomes depend on whether the number of **Success Die** matched or exceeded the action's **Challenge**, and/or if the number of **Trouble Die** matched or exceeded the action's **Risk**:

|                        |Success Die >= Challenge |Success Die < Challenge |
|:-----------------------|:------------------------|:-----------------------|
|**Trouble Die >= Risk** |Limited Success          |Failure                 |
|**Trouble Die < Risk**  |Success                  |Limited Failure         |

For example, a character is trying to steal a guarded item:

- **Success** : the character succeeds, they achieve their intended goal or result without consequences.

>*The character manages to steal the item and remains undetected.*

- **Limited Success** : the character succeeds but it comes at a cost. There's a setback, a complication or the action isn’t as effective as anticipated.

>*The character manages to steal the item but gets detected by guards.*

- **Limited Failure** : the character fails but there's no impactful consequence. The GM may allow them to try again (after some time, or with increased **Risk**) or decide that the opportunity slips away.

>*The character fails at stealing the item but remains undetected.*

- **Failure** : the character fails and there's a consequence. A new threat appears, they lose control of the situation or suffer some form of harm.

>*The character fails at stealing the item and gets detected by guards.*

#### Critical Outcomes

- A **Critical Success** happens when a player manages to roll 2 **Success Die** over the action's **Challenge**. This counts as a *success* with extra bonus or advantage.

- A **Critical Failure** happens when a player scores 2 **Trouble Die** over the action's **Risk**. This counts as a *failure* with a more severe consequence.




## Example


Finn attempts to pick the lock of a chest to steal valuable items. The GM decides that the action requires *dexterity* and *reasoning*, with a **Challenge** of 2 (it's an *average* lock) and a **Risk** of 3 (guards are patrolling the area, it's a *risky* move).

- The player rolls two *d6s* and two *d4s* (based on Finn's *dexterity* of 2 and *reasoning* of 1).

- They get results of 4, 2, 3 and 2. 4 counts as one **Success Die**, both 2s count as two **Trouble Die** and 3 is ignored.

<!-- Briefly introduce Modifiers here...? -->

- Finn has one **Success Die** (not enough to succeed) and two **Trouble Die** (not enough to alert the guards), thus the player can choose to end on a *limited failure*. But instead they decide to push their luck with a **Daring Roll**.

- After raising their *anxiety* by 1 (Finn is scared of being caught), they roll another two die (one *d6* and one *d4*) and get results of 5 and 1 (one more **Success Die** and one more **Trouble Die**).

- Since the total number of **Success Die** (2) matched the **Challenge** and the total number of **Trouble Die** (3) also matched the **Risk**, the action ends in a *limited success*. The GM tells the player that Finn succeeded at picking the lock, but a guard heard them and is now walking towards their location.




<!-- ## Resisting Consequences


**Resistance Rolls** can be used to cancel out negative consequences resulting from *limited successes* and *failures* (converting them into *successes* and *limited failures*, respectively). If so, the character uses the **Ability** corresponding to the **Emotion** used (or hypothetically used) for their **Daring Roll**, and **Pressure** is set by the GM depending on how strong the consequence is:

|Daring Roll's Emotion |Resistance Roll's Ability |
|:--------------------:|:------------------------:|
|Anger                 |Humility                  |
|Frustration           |Insight                   |
|Anxiety               |Composure                 |
|Guilt                 |Composure                 |

>*As usual, the GM has the discretion to determine whether a consequence can be resisted or not.*

#### Example

Let's return to the previous situation. Finn achieved a *limited success* while picking a lock, inadvertently alerting a nearby guard. The GM tells the player that they may perform a **Resistance Roll** against a **Pressure** of 2 (the guard is only checking on some noise) using Finn's *composure* (Finn is scared of being caught). The player accepts:

- Finn's *composure* score is 1, so they roll two d4s.

- They get results of 4 and 3. Finn's *limited success* is upgraded to a *success* and, because there's no **Trouble Die**, there's no increase in *anxiety* either. The GM tells the player that Finn successfully picked the lock and did not alert a single guard. -->




## Advantage & Disadvantage


Sometimes, rules may specify that a character must perform an **Action Roll** using **Advantage** or **Disadvantage**. If a character rolls with **Advantage**, results of 3 now count as **Success Die**. If a character rolls with **Disadvantage**, results of 3 now count as **Trouble Die**.

<!-- Once per day, if a character is not in a Breakdown state, they may add 1 Stress to roll with Advantage on any Action roll thay want (or turn a roll with Disadvantage into a neutral roll). This is to show that characters are able to "force motivate" themselves into doing things that are not always aligned with their goals. -->

#### Drive Actions

For example, characters roll with **Advantage** if they perform an **Action Roll** aligned with at least one of their **Drives**:

- **Authority** : when addressing an obstacle through confrontation or intimidation.

- **Revenge** : when attempting vindication or retaliation against a perpetrator.

- **Wealth** : when tackling a challenge where money or valuables are at stake.

- **Hedonism** : when engaging in a gratifying experience or trying to fulfil a desire.

- **Distinction** : when trying to stand out while addressing an obstacle.

- **Influence** : when trying to solve a problem with persuasive communication or manipulation.

<!-- - **Privilege** : when leveraging their status, connections or when receiving exclusive benefits. -->

- **Solution** : when addressing an obstacle with rationale, technical skill or strategic thinking.

- **Production** : when creating things, like building a contraption or designing a piece of art.

- **Curiosity** : when venturing into the unknown or trying to gain insight into a specific subject.

- **Care** : when providing assistance or support for those in need.

- **Peace** : when mediating disputes or trying to resolve conflicts through reconciliation.

- **Duty** : when carrying out responsibilities to their family, gods or companions.

<!-- This move also gives **XP** to the character...? -->

On the other hand, characters roll with **Disadvantage** if their action goes against at least one of their **Drives** (or current **Quest**).

<!-- or when going against an **Emotion** causing a **Breakdown** state as well? -->

<!-- ### - Example ? -->


<!-- ### - Example ? -->

<!-- Blades in the Dark Playbooks:

- When you play a Cutter, you earn xp when you address a challenge with violence or coercion. Go ahead and get up in everyone’s smug faces and tell them who’s boss, then let your blades do the talking if they don’t get the message.

- When you play a Hound, you earn xp when you address a challenge with tracking or violence. Take the initiative to hunt down opportunities and targets for a score and be willing to hurt whoever stands in your way.

- When you play a Leech, you earn xp when you address a challenge with technical skill or mayhem. Duskwall is a city full of industrial machinery, clockworks, plumbing, and electrical systems for you to bend to your purposes or sabotage. Get out your tools and get your hands dirty.

- When you play a Lurk, you earn xp when you address a challenge with stealth or evasion. Stay out of sight, sneak past your enemies, and strike from the shadows. If things go wrong, there's no shame in disappearing into the darkness... for now. Your greatest ally is the dark and twisting city, its ink-dark streets, its rooftop pathways.

- When you play a Slide, you earn xp when you address a challenge with deception or influence. Talk your way into trouble, then talk your way out again. Pretend to be someone you're not. Bluff, lie, and manipulate to get your way. Every problem is ultimately a problem because of people—and people are the instrument upon which you play your music.

- When you play a Spider, you earn xp when you address a challenge with calculation or conspiracy. Reach out to your contacts, friends, and associates to set your crew up for success. Use your downtime activities and flashbacks wisely to prepare for trouble and to calculate the angles of success. When things go wrong, don't panic, and remember: you planned for this.

- When you play a Whisper, you earn xp when you address a challenge with knowledge or arcane power. Seek out the strange and dark forces and bend them to your will. By being willing to face the trauma from the stress-intensive abilities of the arcane, you'll slowly remove parts of yourself, and replace them with power. -->

***








[**<- Previous**]({{< ref "/docs/game-rules/core-mechanics/ability-rolls.md" >}}) | [**Next ->**]({{< ref "/docs/game-rules/core-mechanics/progress-clocks.md" >}})