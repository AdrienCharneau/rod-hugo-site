---
title: Targeted Checks
bookToc: false
draft: true
weight: 4
---

[![*Cover*](https://i.imgur.com/9kdaEBE.jpg)]({{< ref "/_index.md" >}})








# Targeted Checks




A *targeted check* is an *active check* where a character specifically targets another character or NPC. It involves an intentional effort to affect another individual and determines the outcome of their interaction with them. Characters or NPCs performing *targeted checks* are called *instigators*, whereas characters or NPCs being targeted are called *targets*.

>Examples: "Does the character successfully hit the enemy with a ranged attack?", "Can they persuade the guard to let them through the checkpoint?", "Can they follow an elusive suspect?" etc...

*Targeted checks* use the same resolution process as *active checks*, requiring a character to either match or exceed their *Active ST* with an *initial* and optional *daring roll*.




## Contentious vs Cooperative


There are two types of *targeted checks*: *contentious* and *cooperative*.


### - Contentious checks

*Contentious checks* represent actions where an *instigator* is trying to put their own interests above the ones of their *target*. These checks can be aimed to cause harm, confuse or hinder the progress of other characters or NPCs, but also to protect oneself or secure an advantage. They often involve manipulative tactics or direct confrontation, making them integral to situations where an *instigator*'s survival, personal gain or pleasure is at stake. For example:

- **Attacking** or **dealing a physical blow** requires *strength* and *dexterity*

- **Aiming at a target** requires *dexterity* and *reasoning*

- **Intimidation** requires *eloquence* and *strength*

- **Bargaining** requires *eloquence* and *insight* as it involves convincing the *target* and understanding the value of what is being bought or sold.

- **Sneaking on a target** requires *dexterity* and *agility*

- **Deception**, **coming up with excuses** or **lying about one's intentions** requires *eloquence* and *creativity* (or *knwoldege* if the *instigator* is trying to base their lie on an empirical truth)

- **Seduction** requires *charm* and *eloquence* (*agility* if the *instigator* is doing it through a dance).

- **Argumentation** or **convincing** requires *eloquence* and *reasoning* (or *insight*, depending on what is being argued)


### - Cooperative checks

*Cooperative checks* represent actions where an *instigator* is trying to put a *target*'s interests above their own. These checks encompass a wide range of behaviors designed to foster friendship, save someone from danger or try to enhance their well-being. They involve selfless motives, altruistic actions and empathetic choices, making them integral to situations where a *target*'s vitality, welfare or comfort is at stake. For example:

- **Healing a wound** requires *knowledge* and *reasoning* (and the skill to perform "healing spells" of course)

- **Rescuing someone from a shaft with a rope** requires *strength* and *endurance*

- **Teaching a piece of knowledge** requires *knowledge* and *humility*

- **Teaching a piece of wisdom** requires *insight* and *humility*

- **Perform a dance for entertainment purposes** requires *charm* and *agility*


### - (Other/unclear)

- **Interrogation** requires *eloquence* and *humility*? (vs *insight* or *composure*?)

- **Ridicule, humiliate, making fun of someone, provocation?** requires *eloquence* and *insight*? (vs *composure*?)




## Ward vs Assist modifiers


When performing a *targeted check*, an *instigator* must also add to their *Active ST* a relevant ability's *ward* or *assist modifier* from their *target*. *Ward modifiers* apply to *contentious checks*, whereas *assist modifiers* apply to *cooperative checks*. Choosing the right ability depends on the context or situation for the check.

>For example: "an *instigating* character wants to prevent a *target* character from falling into a ravine". For this action to succeed, it would be preferable for the *target* to have sufficiently high *agility* so that the *instigator* doesn't have to exert too much effort in order to help them out. So the *target*'s *agility assist modifier* should be added to the *instigator*'s *Active ST* for this check.


### - Ward modifiers

*Contentious checks* use an ability's *ward modifier* from a *target* to calculate the *instigator*'s *Active ST*. For example:

- **Target's Agility** : if they are trying to dodge or evade an attack from the *instigator*.

- **Target's Endurance** : if they are trying to parry or absorb a physical blow from the *instigator*.

>If a *target* is wearing *light armor* (or not wearing armor at all), an attack on them will require adding their *agility ward modifier* to the *instigator*'s *Active ST*. On the other hand, if they're wearing *heavy armor*, it will require using their *endurance ward modifier* instead.

- **Target's Dexterity** : if the *instigator* is trying to approach the *target* without being detected or spotted by them.

- **Target's Insight** : if they are trying to call a bluff or detect a lie from the *instigator*, reveal their manipulation or deception. If the *instigator* is including empirically misleading information in their words, the *target*'s *knowledge ward modifier* might be of better use in that context.

- **Target's Composure** : if the *instigator* is trying to exploit the *target*'s primal instincts, emotions, indulgences or vices (by using seduction or intimidation for example).


### - Assist modifiers

*Cooperative checks* use an ability's *assist modifier* from the *target* to calculate an *instigator*'s *Active ST*. For example:

- **Target's Agility** : if the *instigator* is trying to pull the *target* out of a ravine they fell into.

- **Target's Endurance** : if the *instigator* is trying to provide care for the *target*'s physical injuries.

- **Target's Dexterity** : if the *instigator* is trying to show something or make the *target* aware of a danger by screaming or directing visual or auditory attention onto it.

- **Target's Humility** : if the *instigator* is trying to teach or make the *target* understand something that would be beneficial for them (or is at least true - *reasoning* if it's something practical or scientific?). *Humility* is also used when the *instigator* is trying to emotionally *bond* with the *target*.

>Humility should also be used when the *instigator* is trying to display their own artistic talent to the *target* to entertain them? -> yep, good idea imo
