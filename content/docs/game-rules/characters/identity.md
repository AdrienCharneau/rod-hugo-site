---
title: Identity
bookToc: false
draft: true
weight: 2
---

[![*Cover*](https://i.imgur.com/9kdaEBE.jpg)]({{< ref "/_index.md" >}})








# Identity

***

# Name


Name of the character.




# Sex & Gender


Sex and gender of the character.




# Birthplace


Place of birth for the character.




# Culture


The character's upbringing, which defines their language proficiency and familiarity with specific traditions.




# Social Status


The character's societal position or rank in their community. Social status can be *high* or *low*, and is influenced by the character's birthplace (which can be *urban* or *rural*):

- **Bourgeois** : *urban high status*
- **Proletarian** : *urban low status*
- **Noble** : *rural high status*
- **Peasant** : *rural low status*




# Faith


The character's religious beliefs or spiritual convictions.




# Backstory

***








[**<- Previous**]() | [**Next ->**]()