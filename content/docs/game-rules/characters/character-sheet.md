---
title: Character Sheet
bookToc: false
draft: true
weight: 1
---

[![*Cover*](https://i.imgur.com/9kdaEBE.jpg)]({{< ref "/_index.md" >}})








# Character Sheet

***

![*Cover*](https://i.imgur.com/9djtiPH.jpg)


<!--

# Identity


Basic information like name, sex, cultural heritage and origins.

-->


# Avatars


Text

<!-- this represents the character's "high concept", encompassing who they are, what they aspire to achieve, and their emotional vulnerabilities. -->




# Drives


Text




# Statement


Text




# Weaknesses


Text




# Abilities


Text

<!-- the character's aptitude in various aspects of gameplay. -->




# STs


Text




# Health & Stress


Text

<!-- the character's physical and emotional well-being. -->




<!--

# Dispositions


the character's feelings or inclinations towards other characters.

-->

***








[**Next ->**]()