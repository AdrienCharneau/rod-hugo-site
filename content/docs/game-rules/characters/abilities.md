---
title: Abilities
bookToc: false
draft: false
weight: 4
---

[![*Cover*](https://i.imgur.com/9kdaEBE.jpg)]({{< ref "/_index.md" >}})








# **Abilities**

***




Characters have 12 **Abilities** that can be grouped into 4 different domains (*Physical*, *Social*, *Intellectual* & *Wise*):

|Ability    |Domain              |Usage |
|:----------|:-------------------|:-----|
|Strength   |Physical            |*Close combat, breaking things, moving or pushing heavy loads...* |
|Dexterity  |Physical            |*Reflexes, detection, perception, accuracy...* |
|Agility    |Physical & Social   |*Speed, athletics, acrobatics, proficiency in sports...* |
|Charm      |Social              |*Seduction, entertainment, persuading through looks, style or demeanor...* |
|Eloquence  |Social              |*Public speaking, bargaining, persuading through oratory skills...* |
|Creativity |Social & Intellectual |*Thinking outside the box, come up with lies, find solutions to problems or puzzles...* |
|Knowledge  |Intellectual        |*Analyzing, understanding or recalling information, identifying something, learning spells...* |
|Reasoning  |Intellectual        |*Casting spells, computing, calculating, crafting, building or repairing...* |
|Insight    |Intellectual & Wise |*Bargaining, reading a situation or person...* |
|Humility   |Wise                |*Helping, healing, understanding or tolerating others...* |
|Composure  |Wise                |*Maintaining emotional stability, staying calm under pressure, handling stressful situations...* |
|Endurance  |Wise & Physical     |*Stamina, fatigue, resisting physical or mental harm...* |

>*Abilities may be used actively as characters strive to accomplish tasks, or reactively to counteract enemy actions or evade environmental dangers.*

**Physical Abilities** relate to a character's bodily aptitude and physical prowess, encompassing their *strength*, *dexterity*, *agility* and *endurance*. These attributes not only determine their combat effectiveness but also play a vital role in athletic challenges:

- **Strength** represents a character's muscle mass, their capacity to lift, carry or move heavy loads, as well as their effectiveness in melee combat. It is used for close-range attacks, breaking things or lifting heavy weights. It is associated with imposing characters, and can play a role in their aptitude to intimidate others. It is the primary attribute for bodyguards, mercenaries and warriors.

- **Dexterity** symbolizes a character's quick reflexes and precision, measuring their capacity to perform tasks with sleight, hit accurately in combat, detect things, perform quick movements or manipulate objects and devices. It also determines reaction time against surprise attacks or sudden events. It is the primary attribute for sharpshooters, assassins and thieves.

**Social Abilities** relate to a character's social skills and capacity to interact with others, encompassing their *charm*, *eloquence*, *agility* and *creativity*. These attributes are used for navigating social situations and concealing intentions, giving opportunities for information gathering, persuasion or deception:

- **Charm** expresses a character's allure, attractiveness and likability. It measures their aptitude to draw people's attention and gain favors through their looks. It plays a role in first impressions and may determine a character's artistic taste, demeanor or dressing style. It is the primary attribute for performers, entertainers, actors or courtesans.

- **Eloquence** illustrates a character's affability, way of expression and aptitude to communicate. It measures their capacity to speak in a clear and compelling manner, as well as their prowess in rhetoric and argumentation (even for writing letters or poems). It is the primary attribute for bards, writers, diplomats or politicians.

**Intellectual Abilities** represent a character's mental acuity and problem-solving capabilities, encompassing their *knowledge*, *reasoning*, *creativity* and *insight*. These play a crucial role when building and repairing things, deciphering complex information or making decisions based on critical thinking:

- **Knowledge** characterizes the accumulation and recollection of information, reflecting a character's aptitude to recall details about history, science or literature. It is used when identifying or discerning relevant facts or pieces of information, and is the primary attribute for researchers, loremasters, historians or scholars.

- **Reasoning** represents a character's ability to think rationally, understand things and make sound assessments. It plays an important role when solving problems through logic; and is used in analysis, computing or deduction. It is the primary attribute for technicians, craftsmen, administrators or cartographers.

**Wise Abilities** represent a character's shrewdness and mental strength, reflecting their *humility*, *composure*, *endurance* and *insight*. These traits enable them to navigate hardships with discernment, provide guidance or maintain a steady presence in the face of adversity:

- **Humility** expresses a character's ability to consider the feelings of others, be tolerant or act with care and compassion. It illustrates their aptitude to relate to people, provide support and make decisions that benefit not only themselves but also those around them. It is the primary attribute for social activists, healers and teachers.

- **Composure** represents a character's capacity to remain level headed in the face of adversity. It determines how well they handle unfavorable impulses like temptation, disappointment or fear, and is used in situations that require self-control or discipline. It is the primary attribute for ascetics, monks, hermits and priests.

<!-- Equanimity : https://en.wikipedia.org/wiki/Equanimity -->

**Dual Domain Abilities** are part of two domains simultaneously, encompassing *endurance*, *agility*, *creativity* and *insight*. Due to their nature, these attributes provide synergistic advantages, enabling characters to blend their talents and adapt to a variety of situations:

- **Agility** (*Social* & *Physical*) symbolizes a character's movement capabilities, encompassing their speed, body coordination and overall nimbleness. It measures their prowess in scaling, jumping, swimming or dodging attacks. It is the main attribute for martial artists, acrobats, dancers and swashbucklers.

- **Creativity** (*Intellectual* & *Social*) represents a character's aptitude to think outside the box, generate new ideas or come up with innovative solutions. It is used when designing devices, producing original pieces of art, coming up with lies or improvising. It is the primary attribute for inventors, visionaries and artists.

- **Insight** (*Wise* & *Intellectual*) represents a character's maturity and understanding of the world through firsthand experience. It shows their aptitude in making astute observations or remaining skeptical in suspicious situations. It is used for detecting lies, perceiving underlying motives or evaluating the value of goods and services. It is the primary attribute for philosophers, explorers and merchants.

- **Endurance** (*Physical* & *Wise*) embodies a character's toughness and capacity to withstand injuries or pressure, reflecting their overall vitality and mental fortitude. It illustrates their aptitude to survive in harsh environments, navigate through uneven terrain and is the primary attribute for scouts, athletes, soldiers and hunters.




## Ability Scores


Each **Ability** has a score ranging from 1 to 5 and each score is assigned with a dice class:

|Ability Score |Dice Class    |Description |
|:------------:|:------------:|------------|
|1             |d4            |The character is weak or inept in that ability. |
|2             |d6            |The character is intermediate or has basic aptitude in that ability. |
|3             |d8            |The character is above average or has good aptitude in that ability. |
|4             |d10           |The character is very apt, skilled, or competent in that ability. |
|5             |d12           |The character is extremely strong or highly accomplished in that ability. |




## Character Setup


### - Assessing Categories

**Abilities** are grouped into one of three categories (*proficient*, *crippled* or *standard*), depending on the player's choices:

- **Proficient Abilities** are those that a character has an affinity with, allowing for quicker training and development compared to others. They start with a score of 3 <!-- and have a XP track of 6 -->.

- **Crippled Abilities** are those that a character struggles with, holding them back as they cannot be leveled. They always have a score of 1<!-- and no XP track -->.

- Finally, **Standard Abilities** are those that don't fall into any of the other two categories. They start with a score of 2<!-- and have a XP track of 9 -->.

When creating a new character, a player must pick two **Proficient Abilities** and four **Crippled Abilities** for them:

- For **Proficient Abilities**, the choice should be based on the character's **Archetypes**. *Champions* may only pick between *physical* **Abilities**, *icons* between *social* **Abilities**, genius between *intellectual* **Abilities** and *sages* between *wise* **Abilities**.

- For **Crippled Abilities**, the choice is free as long as the character doesn't have more than one **Crippled Ability** aligned with an **Archetype**. For instance, *champions* cannot have more than one crippled *physical* **Ability**, *icons* cannot have more than one crippled *social* **Ability**, genius cannot have more than one crippled *intellectual* **Ability**, and *sages* cannot have more than one crippled *wise* **Ability**.

<!-- This means that new characters have a grand total of 22 Ability points. -->


### - Assessing Total Health

Each character has a maximum **Health** pool value, representing their capacity to withstand physical wounds and remain conscious. This is how it's calculated (starting at 0):

|Ability Score     |Endurance         |Strength          |Agility           |
|:----------------:|:----------------:|:----------------:|:----------------:|
|1                 |total *health* +5 |total *health* +2 |total *health* +1 |
|2                 |total *health* +6 |total *health* +3 |total *health* +2 |
|3                 |total *health* +7 |total *health* +4 |total *health* +3 |
|4                 |total *health* +8 |total *health* +5 |total *health* +4 |
|5                 |total *health* +9 |total *health* +6 |total *health* +5 |

<!-- Provide an example? -->


### - Assessing Emotional Thresholds

**Emotions** represent a characters' psychological vulnerabilities, influencing their behavior and responses to adversity. Each **Emotion** is assigned with a threshold, which determines how much of that **Emotion** a character can hold before entering a **Breakdown** state. This is how these values are established:

- First, each **Emotion** is associated with an **Ability** and an **Archetype** :

|Emotion     |Ability   |Archetype |
|:-----------|:---------|:---------|
|Anger       |Humility  |Champion  |
|Frustration |Composure |Icon      |
|Anxiety     |Composure |Genius    |
|Guilt       |Insight   |Sage      |

- Second, based on the character's **Ability** score associated with the **Emotion**, and if they've picked the associated **Archetype**, a value is assessed:

|Ability Score |Without Archetype |With Archetype |
|:------------:|:----------------:|:-------------:|
|1             |Threshold = 3     |Threshold = 2  |
|2             |Threshold = 4     |Threshold = 2  |
|3             |Threshold = 5     |Threshold = 3  |
|4             |Threshold = 6     |Threshold = 3  |
|5             |Threshold = 7     |Threshold = 4  |

>*During a game, Ability scores may increase and character Archetypes may change. Once a scenario is over, players should recalibrate their total Health and Emotional thresholds to reflect these changes (if any).*

***








[**<- Previous**]({{< ref "/docs/game-rules/characters/archetypes.md" >}}) | [**Next ->**]({{< ref "/docs/game-rules/characters/template-characters.md" >}})