---
title: Archetypes
bookToc: false
draft: false
weight: 3
---

[![*Cover*](https://i.imgur.com/9kdaEBE.jpg)]({{< ref "/_index.md" >}})








# **Archetypes**

***




**Archetypes** are the central aspects of a character, setting their general tone or high concept. When creating a new character, players must choose 2 **Archetypes** out of the 4 available:

- **Champions** thrive for power and specialize in physical abilities.

- **Icons** thrive for fame and specialize in social abilities.

- **Geniuses** thrive for knowledge and specialize in intellectual abilities.

- **Sages** thrive for wisdom and specialize in wise abilities.

**Champions** are natural-born achievers and extremely skilled fighters. They have a firm predisposition for combat, thrive in confrontations and inspire others to respect them. With their unwavering focus and determination, they thrive when forging a path for themselves or when ensuring the protection and prosperity of their group.

>*Champions have great ambition, a strong sense of pride and work tirelessly to defeat their enemies and achieve their goals. Players that choose this Archetype may want their characters to specialize in military expertise or some type of weapon proficiency.*

**Icons** are driven by the desire for social recognition and fame. They excel in fields such as arts, entertainment or politics, and use their skills to inspire, amuse or influence others. Their charismatic presence allows them to effortlessly capture the public's attention, potentially leading them to become influential figures in society.

>*Icons are natural-born performers, captivating people and leaving long lasting impressions wherever they go. Players that choose this Archetype may want their characters to specialize in public speaking, some type of artistic endeavor or being able to entertain people.*

**Geniuses** are educated individuals committed to mastering specific subjects. They can be practitioners, technicians, researchers or scholars, carrying expertise across various disciplines including biology, literature or archaeology. They possess a relentless thirst for knowledge and excel in problem-solving, innovation and creative thinking.

>*Geniuses may be driven by the desire to break established rules, solve problems or understand abstract concepts. Players that choose this Archetype may want their characters to specialize in magic or gain a very strong insight into a specific discipline.*

**Sages** are wise and composed individuals driven by self-discipline and a desire to help, educate or mentor others. They keep their head up in times of uncertainty, using their discernment to navigate hardships and find ways to progress in life without succumbing to despair. Their reflections and understanding of philosophy may also allow them to create new schools of thought.

>*Sages make perfect healers, monks, teachers or druids. Players that choose this Archetype may want their characters to specialize in some form of caretaking discipline or follow a spiritual path.*




## Combining Archetypes


When choosing 2 **Archetypes**, players must consider their character's general goals and ambitions in order to pick the right combination. This can be illustrated with the lives of two historical figures: *Julius Caesar* and *Marcus Aurelius*. Despite their shared influence as Roman emperors, both possessed distinct character traits, leadership styles and historical roles that reflect the approach of **Archetype** combination:

>*Julius Caesar, a charismatic and ambitious leader, exemplified the combination of champion and icon. His rise to power, from a successful general to dictator, showcased his desire for power, influence and popularity among people. He strategically used his oratory skills and military expertise to establish himself as the most prominent figure in ancient Rome.*

>*In contrast, Marcus Aurelius inherited his role of emperor. As a military leader, his purpose centered around upholding justice, fairness, and ethical values, thus embodying the combination of champion and sage. His renowned philosophical writings, such as the collection Meditations, reflected his commitment to stoic principles and the pursuit of wisdom.*

Although both characters can be classified as *champions*, their overall high-concept diverges based on their second **Archetype**. For instance, if a character has the combination of *champion* and *icon*, they might aspire to become a powerful combatant with a desire for fame and glory. Whereas a character that combines *champion* and *sage* may thrive to become a wandering knight guided by principles of wisdom and virtue.


### - Examples

Here's a list of combinations that can be used for new characters. Keep in mind that these are just examples, and players encouraged to come up with their own ideas:

- **Arcanic Scholar** (*Genius* & *Icon*) : this character aspires to become a well-known polymath, mastering multiple disciplines of magic and leaving behind a lasting legacy of discoveries and breakthroughs.

- **Ascetic Healer** (*Sage* & *Genius*) : this character aspires to become a reclusive yet compassionate caregiver with extensive medical knowledge, setting them apart as a trusted authority in the medical field.

- **Itinerant Bard** (*Icon* & *Sage*) : this character aspires to become a charismatic performer who uses their wit, charm, and musical talents to entertain and inspire others, encouraging social change and fostering peace.

- **Notorious Rogue** (*Icon* & *Champion*) : this character aspires to become a master of crime and legendary figure from the underworld, able to outmaneuver authorities and strike fear into the hearts of the rich.

- **War Artifist** (*Genius* & *Champion*) : this character aspires to become a skilled artisan of war machines, blending arcanic knowledge with combat expertise, and harnessing their intellect to shape the course of battles.

- **Warrior Monk** (*Champion* & *Sage*) : this character aspires to become a honorable warrior following the codes of a martial doctrine, roaming the land mastering both physical combat and practicing self-discipline.




## Drives


**Drives** represent a character's motivations, guiding their actions throughout the narrative. Each character must choose 2 **Drives**, with at least 1 **Drive** matching one of their **Archetypes**:

#### Champion

- **Authority** : the character wants to assert dominance and issue commands.

- **Revenge** : the character seeks justice or retribution.

- **Wealth** : the character is driven to amass riches and possessions.

#### Icon

- **Hedonism** : the character prioritizes indulgence and entertainment.

- **Distinction** : the character seeks to impress others and bring attention to themselves.

- **Influence** : the character aims to sway people to their side and mold their opinions.

<!-- - **Privilege** : the character seeks special attention, entitlement or validation. -->

#### Genius

- **Solution** : the character is driven by problem-solving.

- **Production** : the character wants to build or conceive things.

- **Curiosity** : the character is motivated by a thirst for knowledge.

#### Sage

- **Care** : the character wants to help and protect others.

- **Peace** : the character seeks to foster harmony and mend relationships.

- **Duty** : the character is dedicated to fulfilling their obligations.

<!-- By aligning their conduct with these motivations, characters earn XP and gain Advantage when performing actions. -->




## Quest


Each character also has a specific **Quest** that should be a reflection of their **Drives**. This is written as a concise statement, typically one or two sentences, describing a unique objective or aim for the character:

- **Wealth** & **Duty** : *"I want to travel to the mines of Eldoria to negotiate a trade agreement for my family's gemstone business."*

- **Distinction** & **Solution** : *I want to decipher the enigmatic Sandravi script to earn acclaim and notoriety in academic circles."*

- **Authority** & **Care** : *"I want to administer the city of Lüdim, where hunger and repression have plagued the inhabitants for months."*

- **Hedonism** & **Production** : *"I want to experience the serene sunset of Ignicor and compose a poem about it."*

<!-- Wealth & Duty : "I want to accumulate riches to protect my family from a powerful threat." -->

<!-- Distinction & Solution : I want to find a way to solve a scientific conundrum and gain recognition for my work". -->

<!-- Authority & Care : I want to lead by example, protecting vulnerable people and steering them to a better future. -->

<!-- Hedonism & Production : I want to create an enjoyable and innovative art piece, while indulging in life's pleasures. -->

>*Quests should be seen as active goals, involving things a character strives to accomplish rather than things they wish to avoid.*

<!-- When fulfilling (or at least significantly pursuing) their **Quest**, a character clears all of their their **Emotional** strain and earns large amounts of **XP**. -->

<!-- https://www.youtube.com/watch?v=E79DDGdX62I&t=1676s -->

<!-- https://www.reddit.com/r/WhiteWolfRPG/comments/2sz9se/personal_aspirations_supernatural_aspirations/ -->

<!-- HAVE PLAYERS WRITE BELIEFS LIKE IN THE BURNING WHEEL AND MOUSEGUARD https://gamersplane.com/forums/thread/2232/ -->

<!-- Invisible Sun - The Key p.166 -->




<!-- ## Flaws


**Flaws** are negative traits that mirror a character's **Drives**. They are used by the GM to force characters into taking a specific course of action or making unexpected choices. Each **Flaw** is associated with 2 **Drives**, and players must pick 1 **Flaw** for each one of their **Drives** (2 **Flaws** in total):

- **Harsh** (*Authority*, *Revenge*) : the character is excessively severe or critical in their words and actions. Their uncompromising demeanor may create undesired rifts with NPCs or allies.

- **Reckless** (*Revenge*, *Wealth*) : the character lacks tolerance for delays, often acting impulsively without considering consequences, potentially endangering themselves and those around them.

- **Greedy** (*Wealth*, *Hedonism*) : the character is consumed by an insatiable desire for material possessions, often prioritizing personal gain over the well-being of the group.

- **Vain** (*Hedonism*, *Distinction*) : the character is excessively superficial and concerned with their ego, making them easier to be manipulated through flattery or gratification.

- **Paranoiac** (*Distinction*, *Influence*) : the character is plagued by irrational fears and suspicions, often interpreting innocent actions as signs of betrayal or danger.

- **Lazy** (*Influence*, *Solution*) : the character avoids putting in effort, preferring to coast by on minimum exertion and relying on others to solve problems or fulfill obligations.

- **Obsessive** (*Solution*, *Production*) : the character may become fixated on particular tasks, ideas, or goals to the point of detriment, neglecting relationships and other aspects of life.

- **Weird** (*Production*, *Curiosity*) : the character exhibits eccentric or unconventional behavior, unsettling others and making them feel uncomfortable in social situations.

- **Intrusive** (*Curiosity*, *Care*) : the character lacks boundaries. They frequently insert themselves where they are not welcome, prying into others' affairs and invading their privacy.

- **Overinvested** (*Care*, *Peace*) : the character is too affected by the problems and tragedies of others, often to the point of being more concerned with their well-being than their own.

- **Submissive** (*Peace*, *Duty*) : the character consistently defers to others' authority or wishes, leading to exploitation by them or inhibiting the group's ability to assert themselves.

- **Dogmatic** (*Duty*, *Authority*) : the character rigidly adheres to a set of beliefs or principles, refusing to adapt or consider alternative perspectives. -->

<!-- They may also want to impose their ideology onto others. -->

<!-- **Impatient** -->

<!-- **Bitter** (*Revenge*, *Wealth*) -->

<!-- **Superficial** -->

<!-- **Jealous** -->

<!-- - **Arrogant** (*Distinction*, *Influence*) : the character believes themselves to be superior to others. They might underestimate opponents, dismiss valuable advice or refuse to perform tasks they deem unworthy. They may reveal important information by bragging too much. -->

<!-- **Overambitious** -->

<!-- **Anxious** -->

<!-- **Eccentric** -->

<!-- **Smothering** -->

<!-- **Overprotective** -->

<!-- **Naive** (*Care*, *Peace*) -->

<!-- **Overzealous** -->

<!-- **Fanatic** -->

<!-- **Intolerant** -->


<!-- - Certain (all?) **Flaws** should have ability score requirements (for example, a character shouldn't have *arrogant* if they have a *humility* score greater than 2 or something).

- Players should feel free to come up with their own **Flaws**? (as long as these match their chosen **Drives**). -->

<!-- *** -->

***








[**<- Previous**]({{< ref "/docs/game-rules/introduction.md" >}}) | [**Next ->**]({{< ref "/docs/game-rules/characters/abilities.md" >}})