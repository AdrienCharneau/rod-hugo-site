---
title: Skills
bookToc: false
draft: true
weight: 4
---

[![*Cover*](https://i.imgur.com/9kdaEBE.jpg)]({{< ref "/_index.md" >}})








# Skills


Skills do exist, but they're just here to tell players if their character is allowed to resolve a specific task or not (eg do they have the "repair" skill? -> they can attempt a reparation, do they have the "cast" skill -> they can attempt casting a spell, etc...)

<!-- Reminder: swordfighting doesn't solely enable a character to "use swords" (because anyone knows how to deliver a blow with pretty much anything). Instead, this skill focuses on enabling a character to execute specific combat maneuvers such as "disarming opponents", "delivering precise strikes", "backstabbing" etc... -->

<!-- Change this chapter to "advantages" instead of just "skills"?: "advantages" would include skills, achievements (ranks, feats, etc...), assets (things owned like land or cattle) -->