---
title: Template Characters
bookToc: false
draft: false
weight: 5
---

[![*Cover*](https://i.imgur.com/9kdaEBE.jpg)]({{< ref "/_index.md" >}})








# **Template Characters**

***




## Lucian


![*Lucian*](https://i.imgur.com/eIhWYrY.jpg)


- **Archetypes** : *Genius*, *Sage*

- **Drives** : *Solution*, *Care*

- **Quest** : *"I want to develop a treatment for the grey plague, and provide relief to those afflicted by its devastating effects."*

- **Proficient Abilities** : *Reasoning*, *Humility*

- **Crippled Abilities** : *Strength*, *Agility*, *Charm*, *Eloquence*

- **Standard Abilities** : *Dexterity*, *Creativity*, *Knowledge*, *Insight*, *Composure*, *Endurance*

- **Health Pool** : 9

- **Emotional Thresholds** : *Anger* (5), *Frustration* (4), *Anxiety* (2), *Guilt* (2)

>*Lucian is a dedicated healer with strong intellect and compassion. From a young age, he showed an affinity for herbalism and alchemy, while also helping care for the wounded at a local monastery. It wasn't until the grey plague swept across the region that he found his true calling: discover a cure for the disease and alleviate the suffering it caused.*

>*This character possesses a sharp mind, honed through years of rigorous study and practical experience. His keen insightful nature allows him to remain grounded, recognize the limits of his understanding and the importance of collaboration. However, his strength and nimbleness pale in comparison to his acumen, and his reserved personality leads to challenges when trying to command attention.*

<!-- Lucian maintains strong resilience, having experienced first hand the many facets of human suffering. He approaches challenges with calm and confidence, viewing them as opportunities for growth rather than obstacles. But he is also prone to self-doubt, often struggling with issues he can't understand, or feeling overwhelmed by the weight of past decisions. -->

<!-- POINTER -->




## Morwen


![*Lucian*](https://i.imgur.com/hGerJ5G.jpg)


- **Archetypes** : *Champion*, *Icon*

- **Drives** : *Revenge*, *Solution*

- **Quest** : *"I want to find the person who murdered my crew, and make them pay for what they've done."*

<!-- Make this about revenge instead. -->

- **Proficient Abilities** : *Dexterity*, *Eloquence*

- **Crippled Abilities** : *Knowledge*, *Endurance*, *Humility*, *Composure*

- **Standard Abilities** : *Strength*, *Agility*, *Charm*, *Creativity*, *Reasoning*, *Insight*

- **Health Pool** : 10

- **Emotional Thresholds** : *Anger* (2), *Frustration* (2), *Anxiety* (3), *Guilt* (4)

>*Morwen is a rogue who rose from humble beginnings in the streets of Lüdim. While growing up, she took part in various criminal operations, slowly making a name for herself and building a network of trusted allies. However, her life took a dark turn when her hideout was raided, resulting in the death of her crew. She now wants to uncover the culprit and avenge her friends.*

>*Morwen's strength lies in her dexterity and silver tongue, moving through shadows with grace and precision. However, she is not without her vulnerabilities. Beneath her charming facade, she may not always be the most wise or composed individual, with her inner turmoil often manifesting in fits of anger or frustration.*

<!-- Morwen is not without her vulnerabilities. If things don't go her way, her inner turmoil often manifests in fits of frustration. She also harbors a considerable fear of being judged, relentlessly seeking validation or approval from others. Every failure or criticism cuts deep, amplifying her already poor judgmental nature into cycles of anger and denial. -->

***








[**<- Previous**]({{< ref "/docs/game-rules/characters/abilities.md" >}}) | [**Next ->**]({{< ref "/docs/game-rules/core-mechanics/ability-rolls.md" >}})