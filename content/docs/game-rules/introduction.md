---
title: Introduction
bookToc: false
draft: false
weight: 1
---

[![*Cover*](https://i.imgur.com/9kdaEBE.jpg)]({{< ref "/_index.md" >}})








# Introduction

***




## Role-Playing Games


Whether delving into realms of magic or navigating the landscapes of science fiction, RPGs provide a platform for players to immerse themselves into improvised storytelling. With a trusty set of die and character sheets, they assume the roles of characters embarking on adventures, solving intricate mysteries and facing thrilling challenges.

>*In RPGs, players use their imagination to interact with the game world outlined by the game master (or GM). They resolve actions through a combination of spoken dialogue, dice rolls and the application of rules set by the game.*

<!-- One of the key elements is the freedom to shape the story being told, allowing for a personalized gaming experience where players become co-creators of their own narrative. -->

- The **Scenario**, often referred to as "the game" or "the adventure", is the overarching series of events that provide a structure for players to engage in roleplaying. It sets the stage for the characters' journey and presents them with a variety of situations, mysteries and opportunities for exploration.

<!-- A scenario typically includes a setting, a plot and various challenges for them to overcome. It is pre-designed by the GM and unfolds dynamically based on the players' actions and choices. -->

- The **Setting**, or game world, is the fictional environment where a scenario takes place. It encompasses the landscapes, cities, dungeons and other locations that characters can explore and interact with. Presented with its own lore and conflicts, it serves as a canvas for the GM to run their game.

<!-- offering a sense of realism and depth to the adventure that players experience. Each location within the setting is designed to evoke a specific atmosphere and provide opportunities for plot progression. -->

- The **Game master**, also known as the "GM", is the creative mastermind behind every scenario. It is their responsibility to orchestrate the game session, including its characters, conflicts and overall progression. They act as a narrator, describing environments and situations, guiding the story and playing the role of people that players interact with.

<!-- The GM's primary role is to be the steering force of the adventure. They must learn and interpret the rules written in this book, and make judgment calls to ensure a fair and engaging experience for all the players involved. -->

- **Players** are the participants who take on the roles of characters, interacting with the game world and its inhabitants. They assume imaginary identities, making choices and roleplaying deeds to propel the story forward.

<!-- During a game, each player impersonates a unique character with its own motivations and abilities. They are the creative minds behind these virtual identities, responsible for making decisions, carrying strategic thinking and lending their own personalities to the game. -->

- **Characters** are the scenario's protagonists, the fictional personas that players impersonate. They serve as the vessel through which players interact with the setting and story. As the game progresses, they grow and develop with experiences, shaping their backstories and forging connections with the game world and other characters.

<!-- They directly or indirectly  participate in the game world's events, engage with other characters and shape the narrative through their choices. -->

- **Dice rolls** are the fundamental mechanic that introduces elements of chance into the game. Actions or choices executed by characters may require players to roll a set of die. The results obtained, combined with the characters' abilities and relevant game rules, determine the outcome of a specific event.

<!-- Dice rolls inject unpredictability into a scenario, ensuring that no two moments are exactly the same and that players are kept anticipating the end result of each situation. -->

- **Non-playable characters**, or NPCs, are entities within the game world that are controlled by the GM rather than the players. They can serve various roles, such as guides, sources of information, merchants, antagonists or allies.

<!-- NPCs can be allies, adversaries or neutral individuals who interact with characters during the game. They may possess specialized knowledge about the game world that players don't have, providing clues to solve mysteries or navigate treacherous terrain. -->




## Required Tools


<!-- A few elements are required to play. Having these will ensure that they'll be able to impersonate characters, track their progress, and resolve in-game actions according to the rules. -->

- A comprehensive set of **Die**, including d4s, d6s, d8s, d10s and d12s. There should be a minimum of six die for each type to ensure an enjoyable experience. Having more is always better, as it allows to accommodate for larger groups of players.

- One **Character sheet** per player. These will contain all the information needed to roleplay a character, including their abilities, skills and other details. They also keep track of a character's physical and mental states alongside their item inventories.

<!-- - GM cheat sheet : -->

- **Pencils & erasers** are essential tools for filling out character sheets, taking notes and making adjustments during a game.

<!-- They allow for easy editing, ensuring that game-related information remains accurate and up to date with the narrative unfolding. -->

<!-- - This Rulebook provides all the necessary information for both GM and players to engage in the game. It should be be kept accessible during play for easy reference and clarification, as it is the sole resource for understanding the game's rules and guidelines. -->

***








[**Next ->**]({{< ref "/docs/game-rules/characters/archetypes.md" >}})