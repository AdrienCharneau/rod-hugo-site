---
title: Credits
bookToc: false
draft: false
weight: 3
---

[![*Cover*](https://i.imgur.com/9kdaEBE.jpg)]({{< ref "/_index.md" >}})








# Credits

***




#### Gameplay Inspirations

- *Alien/Year Zero Engine by Tomas Härenstam*

- *Blades in the Dark by John Harper*

- *Chronicles of Darkness by Rose Bailey*

- *The Burning Wheel by Luke Crane*

- *D&D 3.5e*

- *Masks by Brendan Conway*

- *The One Ring by Francesco Nepitello and Marco Maggi*

- *The Veil by Fraser Simons*

<!-- - Position (rebranded as **Risk**), non-binary outcomes, **Progress Clocks**, **Stress**... : [*Blades in the Dark*](https://bladesinthedark.com/)

- "Push your luck" mechanic (aka **Daring Rolls**) : [*Alien/Year Zero Engine*](https://freeleaguepublishing.com/games/alien/)

- **Abilities** and character classes (rebranded as **Archetypes**) : [*D&D 3.5e*](https://www.dandwiki.com/wiki/SRD:Ability_Scores)

- Beliefs, Traits, Aspirations... (rebranded as **Drives** and **Quests**) : [*The Burning Wheel*](https://www.burningwheel.com/), [*Chronicles of Darkness*](https://whitewolf.fandom.com/wiki/Chronicles_of_Darkness)

- **Emotions** : [*Masks*](https://magpiegames.com/pages/masks), [*The Veil*](https://samjokopublishing.com/products/the-veil-cyberpunk-ttrpg) -->

<!-- Special thanks to the rpg-forums community -->




#### Atmosphere & Feel

- *Corto Maltese comic series by Hugo Pratt*

- *The Elder Scrolls III: Morrowind by Bethesda Game Studios*

- *Nausicaä of the Valley of the Wind by Hayao Miyazaki*

- *Roadwarden by Moral Anxiety Studio*

<!-- Fallout: New Vegas? -->

<!-- Hugo Pratt? -->




#### Art

Images were generated using [DALL-E 3](https://www.bing.com/images/create) and hosted on [imgur](https://imgur.com/).




#### Website

Made with [HUGO](https://gohugo.io/), using the custom [Book Theme](https://themes.gohugo.io/themes/hugo-book/) by [Alex Shpak](https://github.com/alex-shpak/) and hosted on [GitLab](https://docs.gitlab.com/ee/user/project/pages/).

***
