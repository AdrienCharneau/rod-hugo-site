---
title: Contact
bookToc: false
draft: false
weight: 2
---

[![*Cover*](https://i.imgur.com/9kdaEBE.jpg)]({{< ref "/_index.md" >}})








# Contact

***




- riseofdissent@gmail.com

- Subscribe to [Newsletter](https://docs.google.com/forms/d/15UsLI4rcp4anxRSx5_aREX8q9LqFAlRMhcAezaSjuho)

- Provide [Feedback](https://docs.google.com/forms/d/1Z-Cb9QrMBZ6Yr3_bMIHpZ9L-A3lf8dDxu9DpoZyjvO4)

>*If you're subscribed and wish to unsubscribe, simply send me an empty email with 'unsubscribe' as the subject line (or any similar word).*

***

