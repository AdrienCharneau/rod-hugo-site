# Rise of Dissent Hugo Website




## Info


Images:

>https://imgur.com/user/kidinnu/posts


Cover art dimensions:

>1024x200




## Hugo


### - Installation

Debian:

>sudo apt install hugo


### - Theme

Book:

>https://themes.gohugo.io/themes/hugo-book/

Demo:

>https://hugo-book-demo.netlify.app/docs/shortcodes/section/second-page/

Example folder (in project's root directory):

`themes/hugo-book/exampleSite/content.en/`


### - Backup folder

```
content/
.gitignore
config.toml
README.md
```


### - Setup

Get a backup folder (`rod-hugo-site-backup`) from your drive. Create a new gitlab project and clone it somwhere on your computer. Then place yourself right outside the project's root folder:

>hugo new site --force rod-hugo-site

Add the Hugo-Book theme to the project:

>cd rod-hugo-site

>git submodule add https://github.com/alex-shpak/hugo-book themes/hugo-book

Copy the backup folder's content (`rod-hugo-site-backup/`) into the project's root directory. And remember to add `.gitlab-ci.yml` if you want to host the project on Gitlab Pages (see below).


### - Usage

Run live server (execute in project's root directory):

>hugo server -b localhost:1313




## GitLab Pages


Tutorial:

>https://gohugo.io/hosting-and-deployment/hosting-on-gitlab


Website URL:

>https://AdrienCharneau.gitlab.io/rod-hugo-site/


Follow the CI agent pipeline:

>https://gitlab.com/AdrienCharneau/rod-hugo-site/pipelines


### - Management

1. On the left sidebar, select *Search* or *go to* and find your project.
2. On the left sidebar, select *Deploy > Pages*.
3. Here you'll be able to *Use unique domain* and *Remove pages*.

>!!! For some reason *Use unique domain* was set to true when pushing the initial `.gitlab-ci.yml` file. Remeber to uncheck this setting if the page is redirecting to some weird URL (and clear browser's cache) !!!

More documentation:

>https://docs.gitlab.com/ee/user/project/pages/introduction.html




## "Custom" CSS


I couldn't figure out how to add custom css to the theme, so in order to remove the "light stripes" in markdown tables, open `themes/hugo-book/assets/_markdown.scss` and comment this line:

```


table {
    tr:nth-child(2n) {
      /*background: var(--gray-100);*/
    }
  }
```




## Legacy (couldn't make it work)


### - Install Hugo module

Download and install go

>https://go.dev/doc/install

Initialize a new module

>hugo mod init gitlab.com/AdrienCharneau/riseofdissent_hugo_website

*OR*

>hugo mod init AdrienCharneau.gitlab.io/riseofdissent_hugo_website

Add this to `config.toml`

```
[module]
[[module.imports]]
    path = 'github.com/hugomods/images'
```

Setup/update module

>hugo mod get